exports.run = (client, message, params) => {

  // Empty message handler
  if (params.length < 3) {
    message.channel.send({
        embed: {
          description: `usage: ${config.prefix}username [new username]`
        }
      })
      .catch(e => client.log.error(e));
    return
  }

  if (message.author.id != config.Dev) return;
  client.user.setUsername(`${params}`)
    .then(message.channel.send("New username set."))
    .catch(e => client.log.error(e));
};

exports.conf = {
  name: 'username',
  aliases: [`username`],
  permLevel: 4,
  enabled: true,
  guildOnly: true
};