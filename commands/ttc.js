exports.run = (client, message, params) => {

  if (message.channel.type != `dm`) { // If not in a DM channel, clean the users command.   
    message.delete(500)
  }

  let timeTilReset = timeTil.getReset();
  message.channel.send({
    embed: tools.embed(`Colony Reset: **${timeTilReset.colonyDateDiff}**`)
  }).catch(e => client.log.error(e))

};

exports.conf = {
  name: 'ttc',
  aliases: [`TTH`],
  permLevel: 0,
  enabled: true,
  guildOnly: false
};