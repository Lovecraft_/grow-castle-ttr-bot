exports.run = (client, message, params) => {

  if (message.channel.type != `dm`) { // If not in a DM channel, clean the users command.   
    message.delete(500)
  }

  let timeTilReset = timeTil.getReset();
  message.channel.send({
    embed: tools.embed(`Guild Reset: **${timeTilReset.guildDateDiff}**`)
  }).catch(e => client.log.error(e))

};

exports.conf = {
  name: 'ttr',
  aliases: [`ttr`, `踢踢耳`, `提提艾儿`],
  permLevel: 0,
  enabled: true,
  guildOnly: false
};