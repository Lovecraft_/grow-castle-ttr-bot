exports.run = (client, message, params) => {

  // Empty message handler
  if (params.length < 1) {
    message.channel.send({
        embed: {
          description: `usage: ${config.prefix}avatar [new avatar]`
        }
      })
      .catch(e => client.log.error(e));
    return
  }
  if (message.author.id != config.Dev) return; // If it's not the developer, don't change it.
  client.user.setAvatar(`${params}`)
    .then(message.channel.send("New Avatar set."))
    .catch(e => client.log.error(e))

};

exports.conf = {
  name: 'avatar',
  aliases: [`avatar`],
  permLevel: 4,
  enabled: true,
  guildOnly: false
};