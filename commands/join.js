exports.run = (client, message, params) => {

  const auth2 = `https://discordapp.com/api/oauth2/authorize?client_id=548631658346053632&permissions=67497024&redirect_uri=http%3A%2F%2F192.168.1.1%2F&scope=bot`;

  message.channel.send(`Click the link to invite TTR bot to your own discord server: ${auth2}`)
    .catch(e => client.log.error(e));

};

exports.conf = {
  name: 'join',
  aliases: [`invite`],
  permLevel: 0,
  enabled: true,
  guildOnly: true
};