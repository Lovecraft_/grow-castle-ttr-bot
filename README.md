To install:
- Extract to a folder of your choosing.
- Create config.json in the root directory and use this format:
{ 
    "Dev" : "250647621163548672",
    "token"  : "YOUR_BOT_TOKEN",
    "prefix" : "!"
  }
- Save config.json
- Run: npm install
- Execute: node index.js