// 2019 Lovecraft WTFPL
/**───────────────────────────────────────────────────────────────── @START_CLASS ─────*/
class timetil {
  /**───────────────────────────────────────────────────────────────── @METHOD_UPDATE_STAMPS ─────*/
  updateStamps(stampObject) {
    var data = JSON.stringify(stampObject); // Convert our object to a string
    fs.writeFile('./resetSchedule.json', data, function (err) { // Write the file
      if (err) {
        console.log('There has been an error saving your configuration data.');
        console.log(err.message);
        return;
      }
      console.log('Configuration saved successfully.')
    });
  }
  /**───────────────────────────────────────────────────────────────── @METHOD_GET_STAMPS ─────*/
  getStamps() {
    var data = fs.readFileSync('./resetSchedule.json'), // Read our data (JSON)
      myObj;
    try {
      myObj = JSON.parse(data); // Push the JSON into an Object
      return (myObj); // Return the object.
    } catch (err) {
      console.log('There has been an error parsing your JSON.')
      console.log(err);
    }
  }
  /**───────────────────────────────────────────────────────────────── @METHOD_GET_RESET ─────*/
  getReset() {
    let inputDate = new Date(), // Get our current date
      rootStamp = this.getStamps(), // Get Root TimeStamp object
      // ─────────────────────────────────────────────────────────────────
      dateDiffMS = { // Construct our difference object.
        guildSeason: tools.dateDiffInDays(new Date(Date.UTC(...rootStamp.guildSeason)), inputDate),
        hellSeason: tools.dateDiffInDays(new Date(Date.UTC(...rootStamp.hellSeason)), inputDate),
        colonySeason: tools.dateDiffInDays(new Date(Date.UTC(...rootStamp.colonySeason)), inputDate)
      };
    /**───────────────────────────────────────────────────────────────── @CHECK_RESET ─────*/
    if (dateDiffMS.guildSeason >= 432000) { // Reset for Guild / Personal Season
      let changeStamp = tools.addDays(new Date(Date.UTC(...rootStamp.guildSeason)), 5), // Add 5 days to the current rootStamp
        finalStamp = { // Construct our new TimeStamp and only update.
          guildSeason: tools.dateToArray(changeStamp),
          hellSeason: rootStamp.hellSeason,
          colonySeason: rootStamp.colonySeason
        };
      this.updateStamps(finalStamp) // Write the changes to disk
      console.log(`Guild season is over`);
      // ─────────────────────────────────────────────────────────────────
    } else if (dateDiffMS.hellSeason >= 604800) { // Reset for Hell Season
      let changeStamp = tools.addDays(new Date(Date.UTC(...rootStamp.hellSeason)), 7), // Add 7 days
        finalStamp = { // Construct our new TimeStamp and only update.
          guildSeason: rootStamp.guildSeason,
          hellSeason: tools.dateToArray(changeStamp),
          colonySeason: rootStamp.colonySeason
        };
      this.updateStamps(finalStamp) // Write the changes to disk
      console.log(`Hell season is over`);
      // ─────────────────────────────────────────────────────────────────
    } else if (dateDiffMS.colonySeason >= 864000) { // Reset for Colony Season.
      let changeStamp = tools.addDays(new Date(Date.UTC(...rootStamp.colonySeason)), 10); // Add 10 days
      let finalStamp = { // Construct our new TimeStamp and only update.
        guildSeason: rootStamp.guildSeason,
        hellSeason: rootStamp.hellSeason,
        colonySeason: tools.dateToArray(changeStamp),
      }
      this.updateStamps(finalStamp) // Write the changes to disk
      console.log(`Colony season is over`);
    } // ─────────────────────────────────────────────────────────────────
    let dateDiff = { // Construct a formatted return object.
      guildDateDiff: tools.secondsToDhms((432000 - dateDiffMS.guildSeason)),
      hellDateDiff: tools.secondsToDhms((604800 - dateDiffMS.hellSeason)),
      colonyDateDiff: tools.secondsToDhms((864000 - dateDiffMS.colonySeason))
    }
    return dateDiff; // Return our formatted date object
  };
  /**───────────────────────────────────────────────────────────────── @METHOD_CHECK_TIME ─────*/
  checkTime() {
    let TTR,
      toTTR = this.getReset();
    if (i == 0) { // Alternate the "Now PLaying" text
      TTR = `Guild Reset: ` + toTTR.guildDateDiff;
      i++
    } else if (i == 1) {
      TTR = `Hell Reset: ` + toTTR.hellDateDiff;
      i++
    } else if (i == 2) {
      TTR = `Colony Reset: ` + toTTR.colonyDateDiff;
      i++
    } else if (i == 3) {
      TTR = "Use: !TTR !TTH !TTC !info"
      i = 0;
    }
    client.user.setActivity(TTR); // Set the activity on the bot "Now Playing"
    return TTR; // Return the check
  };
};
// ────────────────────────────────────────────────────────────────────────────────
module.exports = timetil;