module.exports = async (dash, client) => {
  var path = require('path');
  const express = require('express')
  var exphbs = require('express-handlebars');

  dash.engine('handlebars', exphbs({
    defaultLayout: 'main'
  }));
  dash.set('view engine', 'handlebars');

  dash.set('port', process.env.PORT || 3000);

  var options = {
    dotfiles: 'ignore',
    etag: false,
    extensions: ['htm', 'html'],
    index: false
  };
  dash.use(express.static(path.join(__dirname, 'public'), options));
  dash.get('/', function (req, res) {
    res.render('hello'); // this is the important part
  });

  dash.listen(dash.get('port'), function () {
    console.log('express started on http://localhost:' +
      dash.get('port') + '; press Ctrl-C to terminate.');
  });

  dash.get("/client", (req, res) => {
    
    res.end(JSON.stringify(client));
  })
  dash.get("/test", function (req, res) {
    // the "req" parameter is an object that gives you access to data in the
    // request.

    var message = req.query.message;
    var thisFormattedMessage = `
    Username: ${client.user.username}
    ID: ${client.user.id}
    Status: ${client.user.presence.status}
    Playing: ${client.user.presence.game}
    Last Message: ${client.user.lastMessage.content}

    `
    // the "res" parameter lets you manipulate the response.
    res.end(thisFormattedMessage);

  });
};