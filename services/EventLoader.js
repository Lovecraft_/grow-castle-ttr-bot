const reqEvent = (Event) => require(`../events/${Event}`)

module.exports = async (client) => {
  fs.readdir("./events", (err, files) => {
    if (err) return console.error(err);
    files.forEach(file => {
      const event = require(`../events/${file}`);
      let eventName = file.split(".")[0];
      client.on(eventName, event.bind(null, client));
    });
  });
  
  client.on("rateLimit", (rateLimit) => console.log(rateLimit));
  client.on("error", (error) => console.log(error));
  client.on('guildCreate', async (guild) => console.log(`New guild joined: ${guild.name} (id: ${guild.id}). This guild has ${guild.memberCount} members!`))
  client.on('guildDelete', async (guild) => console.log(`Guild Kicked: ${guild.name} (id: ${guild.id}). This guild had ${guild.memberCount} members!`))

};
/*  client.on("", () => console.log());
Available emits are:
READY                       *
RESUMED
GUILD_SYNC
GUILD_CREATE
GUILD_DELETE
GUILD_UPDATE
GUILD_MEMBER_ADD
GUILD_MEMBER_REMOVE
GUILD_MEMBER_UPDATE
GUILD_MEMBERS_CHUNK
GUILD_ROLE_CREATE
GUILD_ROLE_DELETE
GUILD_ROLE_UPDATE
GUILD_BAN_ADD
GUILD_BAN_REMOVE
CHANNEL_CREATE
CHANNEL_DELETE
CHANNEL_UPDATE
CHANNEL_PINS_UPDATE
MESSAGE_CREATE
MESSAGE_DELETE
MESSAGE_UPDATE
MESSAGE_DELETE_BULK
MESSAGE_REACTION_ADD
MESSAGE_REACTION_REMOVE
MESSAGE_REACTION_REMOVE_ALL
USER_UPDATE
USER_NOTE_UPDATE
USER_SETTINGS_UPDATE
PRESENCE_UPDATE
VOICE_STATE_UPDATE
TYPING_START
VOICE_SERVER_UPDATE
RELATIONSHIP_ADD
RELATIONSHIP_REMOVE*/