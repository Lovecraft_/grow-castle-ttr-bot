module.exports = async () => {
  const entities = require('entities'),
    validUrl = require('valid-url');

  snooper.watcher.getPostWatcher('GrowCastle') // blank argument or 'all' looks at the entire website
    .on('post', function (post) {
      let logChannel = client.guilds.get("249006275579346945").channels.find(channel => channel.name === "reddit_posts");
      const embed = new Discord.RichEmbed()
        .setColor('#007cbf')
        .setTitle(`${post.data.link_flair_text ? `[${post.data.link_flair_text}] ` : ''}${entities.decodeHTML(post.data.title)}`)
        .setURL(`https://redd.it/${post.data.id}`)
        .setDescription(`${post.data.is_self ? entities.decodeHTML(post.data.selftext.length > 253 ? post.data.selftext.slice(0, 253).concat('...') : post.data.selftext) : ''}`)
        .setThumbnail(validUrl.isUri(post.data.thumbnail) ? entities.decodeHTML(post.data.thumbnail) : null)
        .setFooter(`${post.data.is_self ? 'self post' : 'link post'} by ${post.data.author}`)
        .setTimestamp(new Date(post.data.created_utc * 1000));
      logChannel.send(embed).then(() => {
        console.log(`Sent message for new post https://redd.it/${post.data.id}`);
      }).catch(err => {
        console.error(embed, err);
      });
    })
    .on('error', console.error)
}