var safeEmbed = require('./safeEmbed');
class tools {
  // 
  // The tools class is written with reuse in mind. 
  // In the future this should contain it's own boilerplate 
  // for interacting with discord without a library.
  //   
  //  Main test
  // 

  test() {
    return "test";
  };
  // 
  // Sends a simple safe embed.
  //   
  embed(message) {
    let sEnbed = new safeEmbed();
    sEnbed.setDescription(message)
    return sEnbed;
  }
  // 
  // Converts seconds to nicely formatted dhms
  //   
  secondsToDhms(seconds) {
    seconds = Number(seconds);
    var d = Math.floor(seconds / (3600 * 24));
    var h = Math.floor(seconds % (3600 * 24) / 3600);
    var m = Math.floor(seconds % 3600 / 60);
    var s = Math.floor(seconds % 3600 % 60);

    var dDisplay = d > 0 ? d + (d == 1 ? " day, " : " days, ") : "";
    var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
    return dDisplay + hDisplay + mDisplay + sDisplay;
  };
  // 
  // Converts seconds to nicely formatted dhms
  //   
  secondsToDhmsX(seconds) {
    seconds = Number(seconds);
    var d = Math.floor(seconds / (3600 * 24));
    var h = Math.floor(seconds % (3600 * 24) / 3600);
    var m = Math.floor(seconds % 3600 / 60);
    var s = Math.floor(seconds % 3600 % 60);

    var dDisplay = d > 0 ? d + (d == 1 ? " 天, " : " 天, ") : "";
    var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
    return dDisplay + hDisplay + mDisplay + sDisplay;
  };
  // 
  // Adds days to date object
  // 
  addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }
  // 
  // Turns a date object into an array
  // 
  dateToArray(a) {
    const utc1 = [a.getUTCFullYear(), a.getUTCMonth(), a.getUTCDate(), a.getUTCHours(), a.getUTCMinutes(), a.getUTCSeconds()];
    return utc1;
  }
  // 
  // Tells us the difference of two times
  // 
  dateDiffInDays(a, b) {
    // Discard the time and time-zone information.
    const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate(), a.getHours(), a.getMinutes(), a.getSeconds());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate(), b.getHours(), b.getMinutes(), b.getSeconds());
    return Math.abs((utc1 - utc2) / 1000);
  }
  // 
  // Resolves an array to a string
  // 
  resolveString(data) {
    if (typeof data === 'string') return data;
    if (data instanceof Array) return data.join('\n');
    return String(data);
  }
  // 
  // Get a users avatar
  // 
  getAvatar(user) {
    var disUser;
    let re = new RegExp(user, 'i');

    client.guilds.get("249006275579346945").roles.find("name", "#1BJ").members.forEach(m => {
      if (user.includes(m.user.username)) {
        console.log("Found discord user match:" + m.user.username)
      }
      if (m.user.username.match(re) != undefined) {
        if (m.user.avatarURL != undefined) { //user has an avatar
          disUser = m.user.avatarURL
        } else { //user does not have an avatar but has a discord account.
          disUser = `https://www.endlessfrontierdata.com/images/units/63.png`
        }
      }
    });
    // Send avatar.
    return disUser;
  };
  // 
  // Return user object from google sheet id
  // 
  getUserfromSheet(rowID) {
    var disUserAvatar;
    var disUser;
    client.guilds.get("249006275579346945").roles.find("name", "#1BJ").members.forEach(m => {
      if (rowID) { // If we have a rowID
        if (m.user.id == rowID) { // See if it matches the user.id
          disUser = m.user; // Set the user to the output.
          console.log(m.user.id); // Print that we found it
          if (m.user.avatarURL) { // Check that user has an avatar
            disUserAvatar = m.user.avatarURL
          } else { // User does not have an avatar but has a discord account. Set default Avatar.
            disUserAvatar = `https://www.endlessfrontierdata.com/images/units/63.png`
          }
        }
      } else { // No discord ID match, set default avatar.
        disUserAvatar = `https://www.endlessfrontierdata.com/images/units/63.png`
      };
    });
    // Send avatar.
    return [disUser, disUserAvatar]; // Return the user.
  };
  // 
  // rewriting discord.js's embed class method.
  // 
  safeEmbed(setTitle,
    setDescription,
    setURL,
    setColor,
    sAuthor,
    setTimestamp,
    addField,
    addBlankField,
    setThumbnail,
    setImage,
    setFooter,
    attachFile,
  ) {
    if (setTitle) { //  If we have a title...
      this.setTitle = setTitle.substring(0, 255); // Safely chomp any titles that are too long.
    }
    if (setDescription) {
      this.setDescription = setDescription.substring(0, 2047); // Safely chomp any descriptions that are too long.
    }
    if (setURL) {
      this.setURL = setURL;
    }
    if (setColor) {
      this.setColor = setColor;
    }
    if (sAuthor) {
      this.sAuthor = this.setAuthor(sAuthor);
    }
    if (setTimestamp) {
      this.setTimestamp = setTimestamp;
    }
    if (addField) {
      this.addField = addField;
    }
    if (addBlankField) {
      this.addBlankField = addBlankField;
    }
    if (setThumbnail) {
      this.setThumbnail = setThumbnail;
    }
    if (setImage) {
      this.setImage = setImage;
    }
    if (setFooter) {
      this.setFooter = setFooter;
    }
    if (attachFile) {
      this.attachFile = attachFile;
    }

    return this;
  }
  // 
  // Fixing discord.js's setAuthor
  //   
  setAuthor(name, icon, url) {
    if (icon || url) {
      this.author = {
        name: this.resolveString(name),
        icon_url: icon,
        url
      };
    } else {
      this.author = {
        name: this.resolveString(name),
        icon_url: undefined
      };
    }
    return this;
  }
  // 
  // Generate a random hex color
  //   
  getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
};
module.exports = tools;