const Attachment = require('../node_modules/discord.js/src/structures/Attachment');
let ClientDataResolver;

/**
 * A safe embed to be sent with a message with a fluent interface for creation. --2018 Lovecraft WTFPL
 * @param {Object} [data] Data to set in the safe embed
 */
class safeEmbed {
  constructor(data = {}) {
    /**
     * Title for this Embed
     * @type {string}
     */
    this.title = data.title;

    /**
     * Description for this Embed
     * @type {string}
     */
    this.description = data.description;

    /**
     * URL for this Embed
     * @type {string}
     */
    this.url = data.url;

    /**
     * Color for this Embed
     * @type {number}
     */
    this.color = data.color;

    /**
     * Author for this Embed
     * @type {Object}
     */
    this.author = data.author;

    /**
     * Timestamp for this Embed
     * @type {Date}
     */
    this.timestamp = data.timestamp;

    /**
     * Fields for this Embed
     * @type {Object[]}
     */
    this.fields = data.fields || [];

    /**
     * Thumbnail for this Embed
     * @type {Object}
     */
    this.thumbnail = data.thumbnail;

    /**
     * Image for this Embed
     * @type {Object}
     */
    this.image = data.image;

    /**
     * Footer for this Embed
     * @type {Object}
     */
    this.footer = data.footer;

    /**
     * File to upload alongside this Embed
     * @type {FileOptions|string|Attachment}
     */
    this.file = data.file;
  }

  /**
   * Sets the title of this embed.
   * @param {StringResolvable} title The title
   * @returns {safeEmbed} This embed
   */
  setTitle(title) {
    title = resolveString(title);
    if (title.length > 256) throw new RangeError('safeEmbed titles may not exceed 256 characters. Soon trunctated => console. ');
    this.title = title;
    return this;
  }

  /**
   * Sets the description of this embed.
   * @param {StringResolvable} description The description
   * @returns {safeEmbed} This embed
   */
  setDescription(description) {
    description = resolveString(description);
    if (description.length > 2048) throw new RangeError('safeEmbed descriptions may not exceed 2048 characters. Soon trunctated ');
    this.description = description;
    return this;
  }

  /**
   * Sets the URL of this embed.
   * @param {string} url The URL
   * @returns {safeEmbed} This embed
   */
  setURL(url) {
    if (url) { //Safety check
      this.url = url;
    }
    return this;
  }

  /**
   * Sets the color of this embed.
   * @param {ColorResolvable} color The color of the embed
   * @returns {safeEmbed} This embed
   */
  setColor(color) {
    if (!ClientDataResolver) ClientDataResolver = require('../node_modules/discord.js/src/client/ClientDataResolver');
    this.color = ClientDataResolver.resolveColor(color);
    return this;
  }

  /**
   * Sets the author of this embed.
   * @param {StringResolvable} name The name of the author
   * @param {string} [icon] The icon URL of the author
   * @param {string} [url] The URL of the author
   * @returns {safeEmbed} This embed
   */
  setAuthor(name, icon, url) {
    if (icon || url) {
      this.author = {
        name: resolveString(name),
        icon_url: icon,
        url
      };
    } else {
      this.author = {
        name: resolveString(name)
      };
    }
    return this;
  }


  /**
   * Sets the timestamp of this embed.
   * @param {Date} [timestamp=current date] The timestamp
   * @returns {safeEmbed} This embed
   */
  setTimestamp(timestamp = new Date()) {
    this.timestamp = timestamp;
    return this;
  }

  /**
   * Adds a field to the embed (max 25).
   * @param {StringResolvable} name The name of the field
   * @param {StringResolvable} value The value of the field
   * @param {boolean} [inline=false] Set the field to display inline
   * @returns {safeEmbed} This embed
   */
  addField(name, value, inline = false) {
    if (this.fields.length >= 25) throw new RangeError('safeEmbeds may not exceed 25 fields.');
    name = resolveString(name);
    if (name.length > 256) throw new RangeError('safeEmbed field names may not exceed 256 characters. Soon trunctated ');
    if (!/\S/.test(name)) throw new RangeError('safeEmbed field names may not be empty.');
    value = resolveString(value);
    if (value.length > 1024) throw new RangeError('safeEmbed field values may not exceed 1024 characters. Soon trunctated ');
    if (!/\S/.test(value)) throw new RangeError('safeEmbed field values may not be empty.');
    this.fields.push({
      name,
      value,
      inline
    });
    return this;
  }

  /**
   * Convenience function for `<safeEmbed>.addField('\u200B', '\u200B', inline)`.
   * @param {boolean} [inline=false] Set the field to display inline
   * @returns {safeEmbed} This embed
   */
  addBlankField(inline = false) {
    return this.addField('\u200B', '\u200B', inline);
  }

  /**
   * Set the thumbnail of this embed.
   * @param {string} url The URL of the thumbnail
   * @returns {safeEmbed} This embed
   */
  setThumbnail(url) {
    if (url) { //Safety check
      this.thumbnail = {
        url
      };
    }
    return this;
  }

  /**
   * Set the image of this embed.
   * @param {string} url The URL of the image
   * @returns {safeEmbed} This embed
   */
  setImage(url) {
    if (url) { //Safety check
      this.image = {
        url
      };
    }
    return this;
  }

  /**
   * Sets the footer of this embed.
   * @param {StringResolvable} text The text of the footer
   * @param {string} [icon] The icon URL of the footer
   * @returns {safeEmbed} This embed
   */
  setFooter(text, icon) {
    text = resolveString(text);
    if (text.length > 2048) throw new RangeError('safeEmbed footer text may not exceed 2048 characters. Soon trunctated ');
    if (icon) { //Safety check
      this.footer = {
        text,
        icon_url: icon
      };
    } else {
      this.footer = {
        text
      };
    }
    return this;
  }

  /**
   * Sets the file to upload alongside the embed. This file can be accessed via `attachment://fileName.extension` when
   * setting an embed image or author/footer icons. Only one file may be attached.
   * @param {FileOptions|string|Attachment} file Local path or URL to the file to attach,
   * or valid FileOptions for a file to attach
   * @returns {safeEmbed} This embed
   */
  attachFile(file) {
    if (this.file) throw new RangeError('You may not upload more than one file at once.');
    if (file instanceof Attachment) file = file.file;
    this.file = file;
    return this;
  }
}

module.exports = safeEmbed;

function resolveString(data) {
  if (typeof data === 'string') return data;
  if (data instanceof Array) return data.join('\n');
  return String(data);
}