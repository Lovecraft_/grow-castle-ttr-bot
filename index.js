/**
 * ───────────────────────────────────────────────────────────────── @GC_TTR_BOT ─────
 * @description: This is a simple "Time til reset bot" for Grow Castle
 * @license WTFPL
 * @author Lovecraft #4690
 */
/**───────────────────────────────────────────────────────────────── @DECLARE ─────*/
const chalk = require('chalk'),
    dash = require('express')();
config = require("./config.json");
Discord = require('discord.js');
client = new Discord.Client();
tools = new(require('./services/tools.js'));
timeTil = new(require('./services/timeTil.js'));
fs = require('fs');
snooper = new(require('reddit-snooper'))({
    automatic_retries: true,
    api_requests_per_minute: 60
})

/**───────────────────────────────────────────────────────────────── @PROCESS ─────*/
require('./services/RedditHandler')(snooper); // Handle reddit listener.
require('./services/EventLoader')(client); // Handle our events.
require('./services/CommandLoader')(client); // Handle our commands.
require('./services/DashHandler')(dash, client); // Handle our dashboard.
client.login(config.token) // Login our client.
    .catch((error) => console.log(chalk.bold(error)));

/**───────────────────────────────────────────────────────────────── @HANDLE_ERRORS ─────*/
process.on("unhandledRejection", err => { // Catch any unhandled errors
    console.error(chalk.bold("Uncaught Promise Error: \n" + err.stack));
});
process.on("error", err => { // Catch any unhandled errors
    console.error(chalk.bold("Uncaught Error: \n" + err.stack));
});