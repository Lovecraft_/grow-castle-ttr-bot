module.exports = client => {

  var TimeStamp = (new Date().toISOString().replace(/.+T/, '[').replace(/\..+/, ']'));
  console.log(`Disconnected, reconnect failed. Attempting to restart client. `);
  client.destroy();
  client.login(config.clientToken)
    .catch(e => client.log.error(e));
  console.log(`${TimeStamp} client has Connected On ` + client.guilds.size + " Servers!");

};