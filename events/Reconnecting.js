module.exports = client => {

  var TimeStamp = (new Date().toISOString().replace(/.+T/, '[').replace(/\..+/, ']'));
  console.log(`Disconnected, reconnecting...`);
  console.log(`${TimeStamp} client has Connected On ` + client.guilds.size + " Servers!");

};