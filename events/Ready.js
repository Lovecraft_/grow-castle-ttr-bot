const chalk = require("chalk"),
  TimeStamp = (new Date().toISOString().replace(/.+T/, '[').replace(/\..+/, ']'));
global.i = 0;

module.exports = async client => {
  console.clear();
  console.log(chalk.blue(`${chalk.yellow(await TimeStamp)} Bot has Connected In ${chalk.red(client.guilds.size)} guilds.`));
  console.log(chalk.blue.bold(`${chalk.white.bold(`:`)}Currently connected to${chalk.white.bold(`:`)}`));

  client.guilds.forEach(guild => {
    console.log(`${chalk.white.bold(guild)} Owned by: ${chalk.white.bold(guild.owner.user.username)}`);
  });

  client.user.setActivity(timeTil.checkTime());
  setInterval(() => {
    client.user.setActivity(timeTil.checkTime());
  }, 30000); // Set the activity

};